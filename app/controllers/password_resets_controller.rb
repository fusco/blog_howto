class PasswordResetsController < ApplicationController
  
  def new
  end


  def create
    # on verifie si on connait le user
    user = User.find_by_email(params[:email])
    if user 
      user.send_password_reset
      flash[:notice] = 'E-mail sent with password reset instructions.'
      redirect_to new_session_path
    else
      flash[:alert] = 'E-mail is not known. Check this and sending newly'
      redirect_to new_password_reset_path
    end
  end

  def edit
    @user = User.find_by_reset_token(params[:id])
    redirect_to new_session_path unless @user
  end

  private

    # Only allow a list of trusted parameters through.
    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation)
    end
end
