class User < ApplicationRecord
  has_many :posts
  has_secure_password
  validates :email, presence: true, uniqueness: true

  before_save :remove_reset_token, if: :will_save_change_to_password_digest?

  # send email with a token
  def send_password_reset
    generate_token(:reset_token)
    save!
    UserMailer.forgot_password(self).deliver 
  end

  # generte random token 
  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  private 
  
  def remove_reset_token
    self.reset_token = nil
  end
end
