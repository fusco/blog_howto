# Création du projet `blog` en mode tuto

## explication

On reprend, l'éternel, exercice du blog un de plus... :blush:

Ce projet est à disposition des apprenants afin de visualiser étape par étape la construction du blog

Chaque grande étape fera l'object d'une branche.

À l'issue de chaque branche, celle-ci  sera `merge` dans `develop`

`readme` avancera en même temps que les branches, il comprendra les commandes à effectuer et les modifications de fichier.

## Installation

__Stack__ :

- ruby 2.6.4
- rails 6.0.x
- sqlite3

Après avoir `clone` le projet, il vous faut renommer le fichier `database.yml.sample` en `database.yml`

rappel pour créer l'app :

```shell
$ rails new blog_howto
  create
  create  README.md
  create  Rakefile
  create  .ruby-version
  create  config.ru
  create  .gitignore
  create  Gemfile
  [...]
  yarn install
  [...]
  ✨  Done in 4.87s.
```

## Lancement

```shell
$ rails db:create
Created database 'db/development.sqlite3'
Created database 'db/test.sqlite3'
```

## C'est partie pour les features

### users  `feature/users`

Création de toute la structure pour gérer les `user`,  le scaffold permet de créer :

- la migration  [db/migrate/yyyymmjjhhmmss_create_users.rb](db/migrate/20200407064648_create_users.rb) 🔗
- le controller [app/controllers/users_controller.rb](app/controllers/users_controller.rb)
- le model [app/model/user.rb](app/models/user.rb) 🔗
- les vues [app/views/users/*.html.erb](app/views/users/*.html.erb) 🔗
- les fichiers de tests  _(et oui, on n'en parle pas maintenant, mais il va fallloir y aller)_
- les assets
- le helper [app/helpers/users_helper.rb](app/helpers/users_helper.rb) 🔗
- les routes [config/routes.rb](config/routes.rb#L2) 🔗

```shell
$ rails g scaffold user name:string email:string:uniq:index
or
$ rails generate scaffold user name:string email:string:uniq:index
```

On peut lancer la migration

```shell
$ rails db:migrate
== 20200407064648 CreateUsers: migrating ======================================
-- create_table(:users)
   -> 0.0022s
-- add_index(:users, :email, {:unique=>true})
   -> 0.0010s
== 20200407064648 CreateUsers: migrated (0.0033s) =============================
```

### installation de Faker pour remplir notre base de données de fausses datas

On édite le fichier `Gemfile` [🔗](Gemfile#L38) et on y ajoute la gem, pas d'obligation de préciser la version, il prendra celle qu'il peut dans les plus récentes, en prenant en compte, les déprendences des autres gems. (bon, des fois, faut le forcer quand même)

```ruby
group :development do
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'faker'
  gem 'web-console', '>= 3.3.0'
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end
```

### Edition du fichier seed

Le fichier [seeds.db](db/seeds.rb) 🔗 nous permet de lancer des commandes ruby pour remplir ou tout au moins interagir avec la bdd.
Dans notre cas nous allons l'utiliser pour mettre de la `sample data` comme on peut l'appeller sur d'autres projets.

```ruby
# delete all entries for moment, i don't use environnement
User.delete_all if User.all.size.positive?

# create 10 users
(1..10).each do |_number|
  User.create name:   Faker::Name.name,
              email:  Faker::Internet.email
end
```

On exécute ce fichier avec

```shell
$ rails db:seed
si pas d'erreur la main est rendu en console
```

### Création des posts

Comme pour `user` on va passer par le scaffold encore une fois.

```shell
$ rails g scaffold post title:string content:text published_at:datetime draft:boolean user:references
Running via Spring preloader in process 36573
      invoke  active_record
      create    db/migrate/20200407191843_create_posts.rb
      create    app/models/post.rb
      [...]
```

`published_at` : permet de savoir si on publie un article ou pas en fonction de la date du jour

`draft`        : permet de savoir si un post est fini ou toujours en brouillon

`user:references` : permet de créer une jointure entre post et user. Ça va créer un champ `user_id` dans post, ainsi qu'une [association](https://guides.rubyonrails.org/association_basics.html) 🌎️ de type `belongs_to`  (un post a un user)

Ça crée à nouveau le model, le controller, le helper, les assets, les tests, la migration... le détail est dans la liste rendu dans la console.

Maintenant il faut lancer la migration, sinon l'application va lever une erreur dans le navigateur.

```log
Migrations are pending. To resolve this issue, run: rails db:migrate RAILS_ENV=development
```

```shell
$ rails db:migrate
== 20200407191843 CreatePosts: migrating ======================================
-- create_table(:posts)
   -> 0.0170s
== 20200407191843 CreatePosts: migrated (0.0171s) =============================
```

Vous pouvez aussi consulter le fichier [db/schema.rb](db/schema.rb) 🔗 qui vous montre le schema de migration de votre application.

**on n'édite pas ce fichier manuellement**, il sera effacé dans un prochain `rails db:migrate`

À cette étape, si on laisse en l'état le formulaire pour saisir un "post", ça n'est pas très user friendly car le fomulaire attend un user_id. Donc l'`id` d'un user dans la base.
Pas simple si on a plusieurs auteurs qui publient.

On va donc modifier le formulaire de `post` afin d'y mettre un `select`

```ruby
 <%= form.select :user_id, @users.collect {|a| [a.name, a.id]} %>
 ```

 `@users` va prendre l'ensemble des users de notre table. Pour cela, il faut l'instancier depuis notre controller. On aurait pu le faire depuis la vue, mais c'est une mauvaise pratique.

 Pour gérer ça depuis notre controller, il y a 2 façon de faire :

- le mettre dans chaque action ou `@users` est nécessaire à la vue avec

 ```ruby
  @users = User.all
 ```

Là, encore, ça fonctionne, mais ce n'est pas optimisé, imaginez que demain vous deviez ajouter une condition à votre demande, il faudrait modifier chaque ligne une à une.

- on va donc privilégier la méthode `before_action` que l'on met en debut de class. [ici](app/controllers/posts_controller.rb#L3) 🔗

```ruby
before_action :set_users, only: %w[new edit update create]
```

et on ajoute la méthode  en dessous de la déclaration de private [ici](app/controllers/posts_controller.rb#L70) 🔗

```ruby
 # load all users
    def set_users
      @users = User.all
    end
```

#### Ajout des posts dans le fichier seed

```ruby
Post.delete_all     if Post.all.size.positive?

# creation des blogs en attibuant un user, et un nombre de tag à chaque.
(1..100).each do |_number|
  Post.create title:        Faker::Lorem.sentence,
              content:      Faker::Lorem.paragraph(sentence_count: 5),
              published_at: Faker::Date.between(from: 2.month.ago, to: 2.month.from_now),
              draft:        rand(0..1),
              user:         User.all.sample
end
```

### Association

Grâçe à la relation [`belongs_to`](https://guides.rubyonrails.org/association_basics.html#the-belongs-to-association) 🌍️ entre le model `user` et `post` il est possible de ressortir l'auteur d'un post.
Par contre en l'état il n'est pas possible de ressortir les posts d'un user.

Pour cela, il faut ajouter une association dans l'autre sens qui s'apelle [`has_many`](https://guides.rubyonrails.org/association_basics.html#the-has-many-association) 🌍️

Modificaiton du model [User](app/models/user.rb#L2) 🔗

```ruby
class User < ApplicationRecord
  has_many :posts
end
```

On peut donc maintenant obtenir l'auteur d'un post

```ruby
irb(main):001:0> @bpost = Post.all.sample
   (0.5ms)  SELECT sqlite_version(*)
  Post Load (0.5ms)  SELECT "posts".* FROM "posts"
=> #<Post id: 60, title: "Quidem impedit sequi nihil.", content: "Error est ut. Alias ratione temporibus. Sequi et s...", published_at: "2020-03-07 00:00:00", draft: true, user_id: 14, created_at: "2020-04-08 20:48:34", updated_at: "2020-04-08 20:48:34">
irb(main):004:0> @post.user
  User Load (0.1ms)  SELECT "users".* FROM "users" WHERE "users"."id" = ? LIMIT ?  [["id", 14], ["LIMIT", 1]]
=> #<User id: 14, name: "Ian Lakin", email: "belia_macgyver@donnelly.co", created_at: "2020-04-08 20:48:34", updated_at: "2020-04-08 20:48:34">
```

Et on peut obtenir les posts d'un user

```ruby
irb(main):005:0> @user = User.all.sample
  User Load (0.2ms)  SELECT "users".* FROM "users"
=> #<User id: 20, name: "Angella O'Connell PhD", email: "salome_bruen@gradyframi.net", created_at: "2020-04-08 20:48:34", updated_at: "2020-04-08 20:48:34">
irb(main):008:0> @user.posts
  Post Load (0.3ms)  SELECT "posts".* FROM "posts" WHERE "posts"."user_id" = ? LIMIT ?  [["user_id", 20], ["LIMIT", 11]]
=> #<ActiveRecord::Associations::CollectionProxy [#<Post id: 1, title: "Qui aut sint culpa.", content: "Impedit provident dolorum. Voluptates enim quia. V...", published_at: "2020-06-05 [...] content: "Porro aperiam debitis. Debitis repellat blanditiis...", published_at: "2020-03-19 00:00:00", draft: true, user_id: 20, created_at: "2020-04-08 20:48:35", updated_at: "2020-04-08 20:48:35">, ...]>
```

On modifie les vues de post et de blog, pour afficher d'un côté les posts d'un user, de l'autre le nom du user sur un post.

[`app/views/posts/show.html.erb`](app/views/posts/show.html.erb#L25) 🔗

```ruby
<p>
  <strong>User:</strong>
  <%= @post.user.name %>
</p>
```

[`app/views/users/show.html.erb`](app/views/users/show.html.erb#L17) 🔗

```ruby
<h2> Les posts </h2>

<table>
  <thead>
    <tr>
      <th>Title</th>
      <th>Content</th>
      <th>Published at</th>
      <th>Draft</th>
    </tr>
  </thead>

  <tbody>
    <% @user.posts.each do |post| %>
      <tr>
        <td><%= post.title %></td>
        <td><%= post.content %></td>
        <td><%= post.published_at %></td>
        <td><%= post.draft %></td>
      </tr>
    <% end %>
  </tbody>
</table>
```

[`app/views/posts/index.html.erb`](app/views/posts/index.html.erb#L24) 🔗

```ruby
<td><%= post.user.name %></td>
```

Cette dernière modification bien qu'intéressante va poser un souci de performance sur le serveur SQL.
En effet sur l'action `index` on va faire une requête pour trouver tous les posts et à l'affichage on va refaire X fois une requête
pour trouver l'auteur d'un post.

Ce phénome porte le nom de query N+1, on en parle [ici](https://blog.heroku.com/solving-n-plus-one-queries) 🌍️

ce n'est pas un souci lié à RAILS en particulier, mais à tout programme faisant ce type de requête.

Pour l'optimiser nous allons modifier notre façon de faire la requête au niveau de notre action index du controller [posts](app/controllers/posts_controller.rb#L7) 🔗

```ruby
def index
    @posts = Post.includes(:user).all
end
```

Pour finir avec les posts, on va modifier la [route](config/routes.rb#L4) 🔗 par défaut quand rien n'est préciser daans l'url.

```ruby
 root "posts#index"
 ```

### Tags

Nous allons ajouter des tags à nos posts. Pour cela, nous allons encore une fois utiliser le scaffold.

On part du postula qu'un post peut avoir plusieurs tags, et donc qu'un tag peut être sur plusieurs posts.

Sur rails, il existe 2 types d'associations pour faire ça :

- [has_many :through](https://guides.rubyonrails.org/association_basics.html#the-has-many-through-association) 🌍️
- [has and belongs to many](https://guides.rubyonrails.org/association_basics.html#the-has-and-belongs-to-many-association) 🌍️

La différence entre les 2 est importante et est un choix de conception de l'applicaiton et de la base de données. vous pouvez lire ces détails [ici](https://guides.rubyonrails.org/association_basics.html#choosing-between-has-many-through-and-has-and-belongs-to-many) 🌍️

Pour faire simple, si l'on doit travailler avec les données de la table qui s'occupe de faire la jointure en y stockant d'autres imformations sur cette relation alors on utilisara une association `has_many :through`

Par contre si notre souci est de "simplement" stocker l'association many-to-many, on utilisera une assocation `has_and_belongs_to_many`  on nomme régulièrement cette assocation `habtm` dans les tutos, docs, forums, S.O. ...

Création de Tag [migration](db/migrate/20200410012420_create_tags.rb)  🔗

```shell
$ rails g scaffold tag name:string:index
    create [...]
```

Création de la table de jointure, il faudra juste éditer le fichier et décommenter l'index. [habtm](db/migrate/20200410012617_create_join_table_posts_tags.rb)  🔗

```shell
$ rails g migration CreateJoinTablePostsTags post tag
    create    db/migrate/20200410012617_create_join_table_posts_tags.rb
```

On migre la bdd

```shell
$ rails db:migrate
== 20200410012420 CreateTags: migrating =======================================
-- create_table(:tags)
   -> 0.0024s
-- add_index(:tags, :name)
   -> 0.0012s
== 20200410012420 CreateTags: migrated (0.0038s) ==============================

== 20200410012617 CreateJoinTablePostsTags: migrating =========================
-- create_join_table(:posts, :tags)
   -> 0.0057s
== 20200410012617 CreateJoinTablePostsTags: migrated (0.0058s) ================
```

Je vous laisse à nouvearu regarder [schema.rb](db/schema.rb) 🔗

Changement du fichier [`seed`](db/seeds.rb#L11) 🔗 et [`seed`](db/seeds.rb#L21) 🔗

```ruby
Post.all.each {|n| n.tags.delete_all}
Tag.delete_all    if Tag.all.size.positive?

# creation de tags
(1..30).each do |_number|
  Tag.create name:   Faker::Lorem.word
end

# creation des blogs en attibuant un user, et un nombre de tag à chaque.
(1..100).each do |_number|
  @post = Post.create title:        Faker::Lorem.sentence,
                      content:      Faker::Lorem.paragraph(sentence_count: 5),
                      published_at: Faker::Date.between(from: 2.month.ago, to: 2.month.from_now),
                      draft:        rand(0..1),
                      user:         User.all.sample
  @post.tags << Tag.all.shuffle.drop(rand(1..Tag.all.size))
```

Il va falloir apporter des modifications afin de prendre en compte l'ensemble du fonctionnement des tags.

#### changement du controller

- controller [post](app/controllers/posts_controller.rb#L78) 🔗
  
  il faut ajouter au niveau du `post_params` le paramêtre du tag.
  comme nous sommes sur une relation `many-to-many` on y ajoutera `tag_ids[]`

  ```ruby
  def blog_params
      params.require(:blog).permit(:title, :content, :published_at, :draft, :user_id, tag_ids:[])
  end
  ```

#### changement des models

- model [tag](app/models/tag.rb#l2) 🔗

```ruby
class Tag < ApplicationRecord
  has_and_belongs_to_many :posts
end
```

- model [post](app/models/post.rb#L3) 🔗

```ruby
class Post < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :tags
end
```

#### modification des vues

Il faut permettre à l'ajout d'un post, pouvoir sélectionner 1 ou plusieurs tags.

Par ailleurs sur l'affichage d'un post en particulier, afficher son ou ses tags.

- formulaire de post

```erb
<div class="field">
    <%= form.label :tag_ids %>
    <%= form.collection_select :tag_ids, Tag.order("name ASC").all, :id, :name, { prompt: "Select an option"}, { multiple: true, class: "form-control" } %>
  </div>
```

- vue d'un post

```erb
<p>
  <strong> Tags: </strong>
  <ul>
    <% @post.tags.each do |tag| %>
      <li><%= tag.name%></li>
    <% end %>
  </ul>
</p>
```

### Commentaires

On souhaite ajouter un système de commentaires à nos articles.

Les commentaires feront référence à un article. Il n'y aura pas de commentaires de commentaires (pour le moment)

Ils seront affichés par ordre croissant. Et biensûr il nous faut pourvoir ajouter un commentaire en consultant un article.

Pour le moment on prendra, sur les commentaires, le nom, l'email et le commentaire.

#### creation du scaffold

```shell
$ rails g scaffold comment name:string email:string content:text post:references
      invoke  active_record
      create    db/migrate/20200414013832_create_comments.rb
      create    app/models/comment.rb
      [...]
```

#### le seed

```ruby
(1..200).each do |_number|
  Comment.create name:    Faker::Name.name,
                 email:   Faker::Internet.email,
                 content: Faker::Lorem.paragraph(sentence_count: 2),
                 post:    Post.all.sample
end
```

On effectue la migration et on relance notre seed

```shell
$ rails db:migrate
== 20200414013832 CreateComments: migrating ===================================
-- create_table(:comments)
   -> 0.0230s
== 20200414013832 CreateComments: migrated (0.0232s) ==========================

$ rails db:seed
  (on récupère le prompt si tout se passe comme prévu)
```

#### model

La relation `belongs_to` a été placé automatiquement par le scaffold mais il est potentiellement intéressant d'ajouter la relation `has_many` sur le model [`Post`](app/models/post.rb#L4) 🔗

```ruby
class Post < ApplicationRecord
  belongs_to :user
  has_and_belongs_to_many :tags
  has_many :comments
end
```

#### le controller

Quand nous allons consulter un post (action [post#show](app/controllers/posts_controller.rb#L12) 🔗, il faudra aussi pourvoir poster un nouveau commentaire, tout en lisant ceux déjà posté.

```ruby
  # app/controllers/posts_controller.rb
  def show
    @comment = Comment.new
  end
```

`@comment` va nous permettre d'utiliser un formulaire directement dans la page `show` de `post`

#### les vues

Sur la vue, nous avons 2 choses à faire, toutes les 2 dans le show de post.

- [Afficher la listes des commentaires déjà postés](app/views/posts/show.html.erb#L46) 🔗. On mettra ça apres les liens `edit` et `back` pour le moment.

```erb
<hr>
<h3>Commentaires</h3>

<% @post.comments.reverse.each do |comment| %>
  <p>name:  <%= comment.name %> </p>
  <p>email:  <%= comment.email %> </p>
  <p>publié le:  <%= comment.created_at %> </p>
  <p>content:  <%= comment.content %> </p>
  <br>
<% end %>
```

- [Affiher le formulaire pour saisir un nouveau commentaire](app/views/posts/show.html.erb#L43) 🔗. Celui ci on le mettra avant la liste des commentaires pour le moment.

Par contre nous allons [utliser un partial](app/views/posts/_comment.html.erb) 🔗, pour voir la différence entre tout mettre dans la vue, et utiliser des partials.

L'utilisation des partials est fortement conseillée pour alléger les vues, et à terme faciliter la conpréhension.

```erb
<%= render 'comment', post: @post, comment: @comment %>
```

Le `post: @post` ou le `comment: @comment` permet de passer des variables locales, à travers le partial, on peut aussi passer des collections mais on verra ça plus tard.

Donc dans notre partial, si on veut utliser l'instance de `post` on utilisera non plus `@post` mais `post`

On crée donc notre partial, ( le nom du fichier d'un partial commence toujours par `_` )

Si un partial n'est pas dans le répertoire de vues du controller qui les apelle, on changera juste son chemin, par exemple

```erb
<%= render 'comments/comment', post: @post, comment: @comment %>
```

Ce partial sera donc attendu dans `app/views/comments/_comment.html.erb`

il faut juste ne pas oublier de passer un [hidden_field](app/views/posts/_comment.html.erb#L2) 🔗 pour l'id de notre post. (il existe d'autres méthodes, pour ne pas devoir s'encombrer de cette démarche et ne pas permettre une édition en console de dev de notre id de post)

```erb
<%= form_with(model: comment, local: true) do |form| %>
  <%= form.hidden_field :post_id, value: post.id %>

  <% if comment.errors.any? %>
    <div id="error_explanation">
      <h2><%= pluralize(comment.errors.count, "error") %> prohibited this comment from being saved:</h2>

      <ul>
        <% comment.errors.full_messages.each do |message| %>
          <li><%= message %></li>
        <% end %>
      </ul>
    </div>
  <% end %>

  <div class="field">
    <%= form.label :name %>
    <%= form.text_field :name %>
  </div>

  <div class="field">
    <%= form.label :email %>
    <%= form.text_field :email %>
  </div>

  <div class="field">
    <%= form.label :content %>
    <%= form.text_area :content %>
  </div>

  <div class="actions">
    <%= form.submit 'ton texte'%>
  </div>
<% end %>
```

Si on laisse les choses en l'état, à la soumission de notre formalaire celui ci sera traité par `comments#create`  ce qui est ce qu'il faut.

Mais dans le controller actuel la réponse sera le `comments#show` au lieu de `posts#show`

On se notera ça pour la suite, quand on aura mis en place une admin.

On pourrait aussi modifier l'index de `comments`  pour afficher les commentaires avec le titre du `post` par exemple.

### Les sessions

Ici nous allons mettre en place des sessions pour authentifier un `user`, ça nous sera bien pratique pour sécuriser notre future administration.

Il nous faut :

- un formulaire de connexion
- un moyen de déconnexion
- le stockage du mot de passe de façon sécuriser
- la possiblité de remettre à zéro son mot de passe

#### installation de `bcrypt`

[`bcrypt`](https://rubygems.org/gems/bcrypt) 🌍️ est déjà présent mais non installé.

Il nous suffit de décommenter la ligne dans notre [Gemfile](Gemfile#L23) 🔗 et de l'installer

```shell
$ bundle install
  Fetching gem metadata from https://rubygems.org/............
  Fetching gem metadata from https://rubygems.org/.
  Resolving dependencies...
  [...]
  Using bcrypt 3.1.13  / Installing bcrypt 3.1.13
  [...]
```

#### modification du model User

On ajoute aussi une nouvelle colonne, `password_digest` en `string` [🔗](db/migrate/20200414114019_add_password_digest_to_user.rb)

```shell
$ rails g migration AddPasswordDigestToUser password_digest:string
      invoke  active_record
      create    db/migrate/20200414114019_add_password_digest_to_user.rb
```

[`has_sesure_password`](https://api.rubyonrails.org/classes/ActiveModel/SecurePassword/ClassMethods.html) 🌍️

Et nous allons en profiter pour ajouter une [validation](https://guides.rubyonrails.org/active_record_validations.html) 🌍️ sur le champ email afin de ne pas se retrouver coincé sur ce champ pour l'authentification.

```ruby
class User < ApplicationRecord
  has_many :posts
  has_secure_password
  validates :email, presence: true, uniqueness: true
end
```

#### creation du controller

Ici nous allons devoir créer un controller, on oublie pas, on reste [`CRUD`](https://fr.wikipedia.org/wiki/CRUD) 🌍️

On va avoir besoin de quoi ?

- connexion via un formulaire
- envoi de la connexion
- destruction de la connexion établie

donc en REST sur RAILS ça nous donne

- NEW
- CREATE
- DESTROY

```shell
$ rails g controller sessions new create destroy
      create  app/controllers/sessions_controller.rb
      route  get 'sessions/new'
      [...]
```

À l'inverse du scaffold la struture de fichier est créé mais elle est peu rempli.

```ruby
class SessionsController < ApplicationController
  def new
  end

  def create
  end

  def destroy
  end
end
```

on va en changer le contenu par [ça](app/controllers/sessions_controller.rb) 🔗

```ruby
class SessionsController < ApplicationController
  def new
  end

  def create
    user = User.find_by_email(params[:email])
    if user && user.authenticate(params[:password])
      session[:user_id] = user.id
      redirect_to root_url, notice: "Logged in!"
    else
      flash.now[:alert] = "Email or password is invalid"
      render "new"
    end
  end
  
  def destroy
    session[:user_id] = nil
    redirect_to root_url, notice: "Logged out!"
  end
end
```

pour les vues on va modifier la vue de [new.html.erb](app/views/sessions/new.html.erb) par  :

```erb
<p id=”alert”><%= alert %></p>

<h1>Login</h1>

<%= form_tag sessions_path do |form| %>
  <div class=”field”>
    <%= label_tag :email %>
    <%= text_field_tag :email %>
  </div>
  <div class=”field”>
    <%= label_tag :password %>
    <%= password_field_tag :password %>
  </div>
  <div class=”actions”>
    <%= submit_tag “Login” %>
  </div>
<% end %>
```

Mais aussi [le form](app/views/users/_form.html.erb#L24) 🔗  de `user` pour prendre en compte ce nouveau champ

```erb
<div class="field">
    <%= form.label :password %>
    <%= form.text_field :password %>
  </div>

  <div class="field">
    <%= form.label :password_confirmation %>
    <%= form.text_field :password_confirmation %>
  </div>
```

Et bien sûr on oublie pas le [permit](app/controllers/users_controller.rb#L) du controller `users`

```ruby
# Only allow a list of trusted parameters through.
def user_params
  params.require(:user).permit(:name, :email, :password, :password_confirmation)
end
```

#### les routes

De base avec notre `rails g controller...`,  3 routes ont été créés

```ruby
  get 'sessions/new'
  get 'sessions/create'
  get 'sessions/destroy'
```

c'est pas très joli, et peu pratique, on va donc y apporter des changements pour :

```ruby
Rails.application.routes.draw do
  resources :sessions, only: %w[new create destroy]
  resources :comments
  resources :tags
  resources :posts
  resources :users
  root "posts#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
```

Et on va se créer 2 routes personnalisées pour se connecter et se déconnecter

```ruby
get 'login', to: 'sessions#new', as: 'login'
get 'logout', to: 'sessions#destroy', as: 'logout'
```

#### current_user

Pour finir, il est pratique de savoir si un utilisateur est connecté ou non, pour nous permettre de proposer des choses différentes en fonction de son état de connexion.

Pour cela on va créer un helper un peu spécial dans le [application_controller.rb](app/controllers/application_controller.rb) 🔗

```ruby
class ApplicationController < ActionController::Base
  helper_method :current_user
  
  def current_user
    if session[:user_id]
      @current_user ||= User.find(session[:user_id])
    else
      @current_user = nil
    end
  end
end
```

La methode [current_user](app/controllers/application_controller.rb#L4) 🔗 déclaré en [helper_method](app/controllers/application_controller.rb#L2) 🔗 va nous permettre de faire des choses comme celle là dans les vues.

```erb
<% if current_user %>
  <%= link_to “Log Out”, logout_path %>
<% else %>
  <%= link_to “Log In”, login_path %>
<% end %>

```

### Reset password

Nous souhaitons maintenant que notre utilisateur peut se connecter, il lui faut la possibilité de remettre à zero son mot de passe.

Pour cela nous allons avoir besoin de :

- formulaire de demande de remise à zéro de son mot de passe
- envoi d'un email avec un lien pour remettre à zero
- formulaire pour changer son mot de passe.

Pour ce faire, on va ajouter une colonne à notre model `User` par convention on va la nommer `reset_token`

#### model User

```shell
$ rails g migration AddResetTokenToUser reset_token:string:index
Running via Spring preloader in process 11987
      invoke  active_record
      create    db/migrate/20200414135502_add_reset_token_to_user.rb
```

et la migration que ça crée automatiquement :

```ruby
class AddResetTokenToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :reset_token, :string
    add_index :users, :reset_token
  end
end
```

Pourquoi mettre un index ? Car quand l'utilisateur va cliquer sur son lien, nous allons devoir checher sur cette colonne pour trouver ou pas une occurence.

On effectue la migration.

```shell
rails db:migrate
```

#### controller

Il nous faut maintenant un controller comme pour les sessions pour permettre à notre utilisateur de saisir son mail et faire la demande d'un mot de passe par mail.
On a besoin :

- d'un formulaire pour l'uilisateur
- de valider que le mail demander est bien en base et envoyer le mail
- un formulaire pour saisir le bon mail quand l'utilisateur aura cliquer sur le lien dans le mail
- la validation du nouveau mot de passe

ce qui donne toujours en CRUD

- NEW
- CREATE
- EDIT
- UDPATE

```shell
rails g controller password_resets new create edit update
```

Comme pour sessions, nous allons modifier les [routes](config/routes.rb) 🔗 pour avec quelque chose de plus propre.

```ruby
resources :password_resets, only: %[wnew create edit update]
```

Pas besoin cette fois de créer des routes `get` personnalisées

#### le controller de passord_resets

- [create](app/controllers/password_resets_controller.rb#L7)

```ruby
def create
    # on verifie si on connait le user
    user = User.find_by_email(params[:email])
    if user
      user.send_password_reset
      flash[:notice] = 'E-mail sent with password reset instructions.'
      redirect_to new_session_path
    else
      flash[:alert] = 'E-mail is not known. Check this and sending newly'
      redirect_to new_password_reset_path
    end
  end
```

On cherche l'utilisateur par son mail, si on le trouve on utilise la méthode du model `User` [send_password_reset](app/models/user.rb)

#### les vues de password resets

La vue `new`  [app/views/password_reset/new.html.erb](app/views/password_reset/new.html.erb) 🔗

```erb
<h1>Reset Password</h1>

<%= form_tag password_resets_path, :method => :post do %>
  <div class='field'>
    <%= label_tag :email %>
    <%= text_field_tag :email, params[:email] %>
  </div>
    <div class='actions'>
    <%= submit_tag "Reset Password" %>
  </div>
<% end %>
```

#### le model

On ajoute les méthodes pour générer et envoyer le mail avec le mot de passe.

```ruby
 # send email with a token
  def send_password_reset
    generate_token(:reset_token)
    save!
    UserMailer.forgot_password(self).deliver
  end

  # generte random token
  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end
```

#### le mailer

On crée le mail pour user afind 'envoyer le mail de remise à zero du password.

```shell
$ rails g mailer user_mailer  forgot_password
 create  app/mailers/user_mailer.rb
      invoke  erb
      create    app/views/user_mailer
      create    app/views/user_mailer/forgot_password.text.erb
      create    app/views/user_mailer/forgot_password.html.erb
      invoke  test_unit
      create    test/mailers/user_mailer_test.rb
      create    test/mailers/previews/user_mailer_preview.rb
```

ça nous crée le mailer pour user et surtout pour la méthode `forgot_password` avec 2 templates, l'un en [html](app/views/user_mailer/forgot_password.html.erb) 🔗  et l'autre en [txt](app/views/user_mailer/forgot_password.text.erb) 🔗

Dans la [méthode du mailer](app/mailers/user_mailer.rb#L8) 🔗 on va faire simple, même si on peut y passer une multitude d'options, (fichier joint, bcc, cc etc)

```ruby
 def forgot_password(user)
    mail to: user.email, :subject => 'Reset password instructions'
  end
```

Je vous laisse regarder le contenu des 2 templates du mailer. avec les liens ci-dessus.

Dernière configuration à réaliser pour pouvoir envoyer des emails. comme nous allons utiliser les `url` au lieu du `path`.

On effectue cette configuration dans [application.rb](config/application.rb#L20) 🔗 en ajoutant

```ruby
# Configure default URL for action mailer
config.action_mailer.default_url_options = {:host =>'localhost:3000'}
```

à la validation d'un formaulaire de demande mot de passe, on put voir ceci arriver dans les logs.  Au dela de voir la requête s'effectuer, on peut aussi y voir notre mail.

```log
Started POST "/password_resets" for 127.0.0.1 at 2020-04-14 20:30:40 +0200
   (0.5ms)  SELECT sqlite_version(*)
   (0.1ms)  SELECT "schema_migrations"."version" FROM "schema_migrations" ORDER BY "schema_migrations"."version" ASC
Processing by PasswordResetsController#create as HTML
  Parameters: {"authenticity_token"=>"W0Fz1T20QFtGHsZGOVhINhWUjweky36tR1YdUcCWjLY9IWVQIUORXrGmjLzeWFNu9sGq0sKSfKpjRiWP0aeIag==", "email"=>"david@pop.eu.com", "commit"=>"Reset Password"}
  User Load (0.2ms)  SELECT "users".* FROM "users" WHERE "users"."email" = ? LIMIT ?  [["email", "david@pop.eu.com"], ["LIMIT", 1]]
  ↳ app/controllers/password_resets_controller.rb:9:in `create'
  User Exists? (0.2ms)  SELECT 1 AS one FROM "users" WHERE "users"."reset_token" = ? LIMIT ?  [["reset_token", "PoDJgYqBzS-P5SgyHHF9-A"], ["LIMIT", 1]]
  ↳ app/models/user.rb:17:in `generate_token'
   (0.0ms)  begin transaction
  ↳ app/models/user.rb:9:in `send_password_reset'
  User Exists? (0.1ms)  SELECT 1 AS one FROM "users" WHERE "users"."email" = ? AND "users"."id" != ? LIMIT ?  [["email", "david@pop.eu.com"], ["id", 31], ["LIMIT", 1]]
  ↳ app/models/user.rb:9:in `send_password_reset'
  User Update (0.4ms)  UPDATE "users" SET "reset_token" = ?, "updated_at" = ? WHERE "users"."id" = ?  [["reset_token", "PoDJgYqBzS-P5SgyHHF9-A"], ["updated_at", "2020-04-14 18:30:40.500178"], ["id", 31]]
  ↳ app/models/user.rb:9:in `send_password_reset'
   (0.7ms)  commit transaction
  ↳ app/models/user.rb:9:in `send_password_reset'
  Rendering user_mailer/forgot_password.html.erb within layouts/mailer
  Rendered user_mailer/forgot_password.html.erb within layouts/mailer (Duration: 0.8ms | Allocations: 292)
  Rendering user_mailer/forgot_password.text.erb within layouts/mailer
  Rendered user_mailer/forgot_password.text.erb within layouts/mailer (Duration: 0.3ms | Allocations: 155)
UserMailer#forgot_password: processed outbound mail in 9.0ms
Delivered mail 5e960150822d7_7a3b3fe0a6f9080043720@MacBook-Pro.mail (7.7ms)
Date: Tue, 14 Apr 2020 20:30:40 +0200
From: from@example.com
To: david@pop.eu.com
Message-ID: <5e960150822d7_7a3b3fe0a6f9080043720@MacBook-Pro.mail>
Subject: Reset password instructions
Mime-Version: 1.0
Content-Type: multipart/alternative;
 boundary="--==_mimepart_5e960150813dd_7a3b3fe0a6f90800436f";
 charset=UTF-8
Content-Transfer-Encoding: 7bit


----==_mimepart_5e960150813dd_7a3b3fe0a6f90800436f
Content-Type: text/plain;
 charset=UTF-8
Content-Transfer-Encoding: quoted-printable

Demande de remise a z=C3=A9ro du mot de passe=0D
=0D
=0D
Vous avez oubli=C3=A9 votre mot de passe, cliquez sur ce lien pour le r=C3=
=A9initialiser http://localhost:3000/password_resets/PoDJgYqBzS-P5SgyHHF9=
-A/edit=0D
=0D
=0D

----==_mimepart_5e960150813dd_7a3b3fe0a6f90800436f
Content-Type: text/html;
 charset=UTF-8
Content-Transfer-Encoding: quoted-printable

<!DOCTYPE html>=0D
<html>=0D
  <head>=0D
    <meta http-equiv=3D"Content-Type" content=3D"text/html; charset=3Dutf=
-8" />=0D
    <style>=0D
      /* Email styles need to be inline */=0D
    </style>=0D
  </head>=0D
=0D
  <body>=0D
    <h1> Demande de remise a z=C3=A9ro du mot de passe</h1>=0D
=0D
<p>=0D
  Vous avez oubli=C3=A9 votre mot de passe, cliquez sur ce lien pour le r=
=C3=A9initialiser http://localhost:3000/password_resets/PoDJgYqBzS-P5SgyH=
HF9-A/edit=0D
</p>=0D
=0D
=0D
  </body>=0D
</html>=0D

----==_mimepart_5e960150813dd_7a3b3fe0a6f90800436f--

Redirected to http://localhost:3000/sessions/new
```

#### gérer le clic d'un utilisateur sur le lien de remise à jour du mot de passe

C'est le controller [password_resets de l'action edit](app/controllers/password_resets_controller.rb#L20) qui va prendre en charge la demande de notre utilisateur.

On va devoir vérifier si potentiellement on trouve un utilisateur qui a cette reset_token,  si oui, on va lui proposer le formulaire pour mettre à jour son mot de passe.

```ruby
def edit
  @user = User.find_by_reset_token(params[:id])
  redirect_to new_session_path unless @user
end
```

À la validation de celui ci, c'est le controller `user#update` qui va prendre en charge la mise à jour du mot de passe.

Par contre il nous reste un élément qu'il faut encore traiter.  *Remettre à nil la reset_token*

Pour ce faire 2 solutions :

- modifier des controllers et le formulaire pour passer par un passowrd_resets#update
- utliser les outils de rails à notre disposition, nommé les [callbacks](https://guides.rubyonrails.org/v2.3.11/activerecord_validations_callbacks.html#callbacks-overview) 🌍️

C'est bien entendu la seconde méthode qui sera privilégier.

Pour se faire on va modifer notre model `User` comme ci

```ruby
class User < ApplicationRecord
  has_many :posts
  has_secure_password
  validates :email, presence: true, uniqueness: true

  before_save :remove_reset_token, if: :will_save_change_to_password_digest?

  # send email with a token
  def send_password_reset
    generate_token(:reset_token)
    save!
    UserMailer.forgot_password(self).deliver
  end

  # generte random token
  def generate_token(column)
    begin
      self[column] = SecureRandom.urlsafe_base64
    end while User.exists?(column => self[column])
  end

  private

  def remove_reset_token
    self.reset_token = nil
  end
end
```

⚠️  *ATTENTION* Notre application est à minima sécurisé mais il faut encore pousser plus loin avec :

- Blocage de compte X minutes si N tentative de connexion ont echouées
- limiter le temps pour remettre son mot de passe à jour
- limiter le nombre de demande de reset de mot de passe sur un temps donné

### SMTP

Maintenant que nous avons tout ce qu'il nous faut, il nous reste à envoyer le mail de reset_password. Nos méthodes pour envoyer le mail sont opérationnelles.

Il suffit maintenant de configurer la partie SMTP.

Pour le développement nous allons utiliser [maildev](http://maildev.github.io/maildev/) 🌍️  Ça nous servira de serveur SMTP et de client IMAP.

```shell
$ npm install -g maildev
  /usr/local/bin/maildev -> /usr/local/lib/node_modules/maildev/bin/maildev
  + maildev@1.1.0
  updated 1 package in 4.179s
```

pour le lancer

```shell
$ maildev
MailDev webapp running at http://0.0.0.0:1080
MailDev SMTP Server running at 0.0.0.0:1025
```

Comme vous pouvez le lire, ça a démarré 2 services l'un sur le `port 1025` pour le `serveur STMP`, l'autre pour sur le `port 1080` pour `l'interface web` pour consulter les mails reçus.

Il nous reste maintenant à configurer notre application rails à utliser pour la partie développement l'application `maildev`

on va pour cela éditer les 2 fichiers de configurations des environnements de [`development`](config/environments/development.rb)  🔗 et de [`production`](config/environments/production.rb) 🔗

[configuration pour l'environnement de development](config/environments/development.rb#L64)  🔗

```ruby
# maildev configuration
  config.action_mailer.delivery_method = :smtp
  config.action_mailer.smtp_settings = {
    address: "localhost",
    port: 1025
  }
```

[configuration pour l'environnement de production](config/environments/production.rb) 🔗

```ruby
config.action_mailer.delivery_method = :smtp
config.action_mailer.smtp_settings = {
  address:              'smtp.gmail.com',
  port:                 587,
  domain:               'example.com',
  user_name:            '<username>',
  password:             '<password>',
  authentication:       'plain',
  enable_starttls_auto: true
}
```

On relance notre serveur rails.

```shell
rails s
```

Et on va demander la remise à zéro de notre pot de passe. [http://localhost:3000/password_resets/new](http://localhost:3000/password_resets/new) 🌍️

je vous laisse admirer sur [http://0.0.0.0:1080/#/](http://0.0.0.0:1080/#/) 🌍️

Maintenant que nous savons que notre mail et que le lien envoyé fonctionne, on va voir comment il est possible de mettre en page notre mail sans devoir en envoyer 50_000.

Raisl a prévu de nous aider graçe au [`preview`](https://guides.rubyonrails.org/action_mailer_basics.html#previewing-emails) 🌍️

Pour visualiser notre mail il suffit d'aller sur [http://localhost:3000/rails/mailers/user_mailer/forgot_password](http://localhost:3000/rails/mailers/user_mailer/forgot_password) 🌍️.
