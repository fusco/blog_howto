Rails.application.routes.draw do
  
  resources :password_resets, only: %w[new create edit update]
  resources :sessions,        only: %w[new create destroy]
  resources :comments
  resources :tags
  resources :posts
  resources :users

  get 'login', to: 'sessions#new', as: 'login'
  get 'logout', to: 'sessions#destroy', as: 'logout'
  root "posts#index"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
