# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# delete all entries for moment, i don't use environnement

# on supprime les relations entre les posts et les tags, si on se contente d'effacer les posts et les tags séparement, la relation va rester.
Post.all.each {|n| n.tags.delete_all}
Comment.delete_all  if Comment.all.size.positive?
Post.delete_all     if Post.all.size.positive?
Tag.delete_all      if Tag.all.size.positive?  
User.delete_all     if User.all.size.positive?


# creation de 10 utilisateurs
(1..10).each do |_number|
  password = Faker::Crypto.md5
  User.create name:                   Faker::Name.name,
              email:                  Faker::Internet.email,
              password:               password,
              password_confirmation:  password
end

# creation de tags
(1..30).each do |_number|
  Tag.create name:   Faker::Lorem.word
end

# creation des blogs en attibuant un user, et un nombre de tag à chaque.
(1..100).each do |_number|
  @post = Post.create title:        Faker::Lorem.sentence,
                      content:      Faker::Lorem.paragraph(sentence_count: 5),
                      published_at: Faker::Date.between(from: 2.month.ago, to: 2.month.from_now),
                      draft:        rand(0..1),
                      user:         User.all.sample
end

Post.all.each do |post|
  post.tags << Tag.all.shuffle.drop(7)
end

(1..200).each do |_number|
  Comment.create name:    Faker::Name.name,
                 email:   Faker::Internet.email,
                 content: Faker::Lorem.paragraph(sentence_count: 2),
                 post:    Post.all.sample
end
                 